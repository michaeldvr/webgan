'''
ref: https://github.com/tensorflow/tensorflow/blob/master/tensorflow/contrib/learn/python/learn/datasets/mnist.py
'''

import numpy as np
from tensorflow.examples.tutorials.mnist import input_data
import os
import pickle

class MNIST(object):

    def __init__(self, datadir='MNIST_data/', one_hot=True, pad=None, seed=None, scale=False, flatten=False):
        '''
        load MNIST data
        :param datadir: save directory
        :param one_hot: one hot vector label
        :param pad: zero pad on images -> ((0,0), (a,b), (c,d), (0,0))
        :param seed: seed for shuffle
        :param scale: scale to [-1, 1]
        :param flatten: flat data to vector
        '''
        self._data = input_data.read_data_sets(datadir, validation_size=0, one_hot=one_hot)
        self.images = np.concatenate((self._data.train.images, self._data.test.images), axis=0)\
            .reshape((-1, 28, 28, 1))
        self.labels = np.concatenate((self._data.train.labels, self._data.test.labels), axis=0)
        if pad is not None:
            self.images = np.pad(self.images, pad, mode='constant')
        self.seed = seed
        if scale:
            self.images = self.images * 2. - 1.
        if seed is not None:
            np.random.seed(seed)
        if flatten:
            self.images = self.images.reshape([-1] + [np.prod(self.images.shape[1:])])
        # private
        self.num_examples = self.images.shape[0]
        self._cur_idx = 0
        self._epochs = 0

    def next_batch(self, batch_size, shuffle=True):
        start = self._cur_idx
        # shuffle first epoch
        if self._epochs == 0 and start == 0 and shuffle:
            indices = np.arange(self.num_examples)
            np.random.shuffle(indices)
            self.images = self.images[indices]
            self.labels = self.labels[indices]
        if start + batch_size > self.num_examples:
            self._epochs += 1
            remainder = self.num_examples - start
            images_0 = self.images[start:self.num_examples]
            labels_0 = self.labels[start:self.num_examples]
            if shuffle:
                indices = np.arange(self.num_examples)
                np.random.shuffle(indices)
                self.images = self.images[indices]
                self.labels = self.labels[indices]
            start = 0
            self._cur_idx = batch_size - remainder
            end = self._cur_idx
            images_1 = self.images[start:end]
            labels_1 = self.labels[start:end]
            return np.concatenate((images_0, images_1), axis=0), np.concatenate((labels_0, labels_1), axis=0)
        else:
            self._cur_idx += batch_size
            end = self._cur_idx
            return self.images[start:end], self.labels[start:end]


class FaceDataset(object):

    def __init__(self, datadir, filelist_path, validation_size=0, seed=None, verbose=True):
        self.seed = seed
        if seed is not None:
            np.random.seed(seed)
        with open(filelist_path, 'rb') as f:
            filelist = pickle.load(f)
        self.filelist = [os.path.join(datadir, f) for f in filelist]
        np.random.shuffle(self.filelist)
        self.num_examples = len(self.filelist)
        if validation_size > 0:
            self.validation_set = np.array(self.filelist[:validation_size]).astype(str)
            self.training_set = np.array(self.filelist[validation_size:]).astype(str)
        else:
            self.validation_set = np.array([]).astype(str)
            self.training_set = np.array(self.filelist[:]).astype(str)
        self.training_size = len(self.training_set)
        self.validation_size = len(self.validation_set)
        self._cur_idx = 0
        self._epochs = 0
        self._cur_val_idx = 0
        self._val_epochs = 0
        if verbose:
            print("===[Dataset]============================")
            print("Dataset folder:", datadir)
            print("Total:", self.num_examples, "| Train:", self.training_size, "| Val:", self.validation_size)

    def next_batch(self, batch_size):
        start = self._cur_idx
        if self._epochs == 0 and start == 0:
            indices = np.arange(self.training_size)
            np.random.shuffle(indices)
            self.training_set = self.training_set[indices]
        if start + batch_size > self.training_size:
            self._epochs += 1
            remainder = self.training_size - start
            files_0 = self.training_set[start:self.training_size]
            indices = np.arange(self.training_size)
            np.random.shuffle(indices)
            self.training_set = self.training_set[indices]
            start = 0
            self._cur_idx = batch_size - remainder
            end = self._cur_idx
            files_1 = self.training_set[start:end]
            return np.concatenate((files_0, files_1))
        else:
            self._cur_idx += batch_size
            end = self._cur_idx
            return self.training_set[start:end]

    def next_val_batch(self, batch_size):
        if self.validation_size == 0:
            return []
        else:
            start = self._cur_val_idx
            if self._val_epochs == 0 and start == 0:
                indices = np.arange(self.validation_size)
                np.random.shuffle(indices)
                self.validation_set = self.validation_set[indices]
            if start + batch_size > self.validation_size:
                self._val_epochs += 1
                remainder = self.validation_size - start
                files_0 = self.validation_set[start:self.validation_size]
                indices = np.arange(self.validation_size)
                np.random.shuffle(indices)
                self.validation_set = self.validation_set[indices]
                start = 0
                self._cur_val_idx = batch_size - remainder
                end = self._cur_val_idx
                files_1 = self.validation_set[start:end]
                # print(type(files_0), len(files_0), type(files_1), len(files_1))
                return np.concatenate((files_0, files_1))
            else:
                self._cur_val_idx += batch_size
                end = self._cur_val_idx
                return self.validation_set[start:end]



def test():
    data = MNIST()
    epochs = 20
    batch_size = 64
    step = data.num_examples // batch_size
    maxctr = int(epochs * (data.num_examples // batch_size))
    print('epochs', epochs, '| batch size', batch_size, '| step', step, '|maxctr', maxctr)
    for epoch in range(1, epochs+1):
        for ctr in range(1, step + 1):
            print('ctr', (ctr+1), '; epoch', epoch)
            _, _ = data.next_batch(batch_size)

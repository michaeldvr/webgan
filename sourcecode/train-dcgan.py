#!/usr/bin/env python3

# Original Version: Taehoon Kim (http://carpedm20.github.io)
#   + Source: https://github.com/carpedm20/DCGAN-tensorflow/blob/e30539fb5e20d5a0fed40935853da97e9e55eee8/main.py
#   + License: MIT
# [2016-08-05] Modifications for Inpainting: Brandon Amos (http://bamos.github.io)
#   + License: MIT

import os
import numpy as np
from model import DCGAN
import tensorflow as tf
from shutil import copy2
import time
import pathhelper as P

LOCAL_RUN = False
# EVALUATE_ON_COMPLETE = True
PAPERSPACE_METRICS = True
PS_METRICS_INTERVAL = 10
OUT_DIR = 'default_20ep_celeba'
DATASET_NAME = 'celeba'  # celeba or lfw
SEED = 2019
VAL_SIZE = 19699
# Config & HyperParam
EPOCHS = 20
LEARNING_RATE = 0.0002
BETA1 = 0.5
BATCH_SIZE = 64
IMAGE_SIZE = 64
DESCRIPTION =\
"""[default] Training python3, tensorflow 1.4
epochs 20 @celeba dataset 64px aligned
running on paperspace"""

if LOCAL_RUN:
    if DATASET_NAME == 'celeba':
        DATA_DIR = P.LOCAL_CELEBA_ROOT
        FILE_LIST = P.LOCAL_CELEBA_LIST
    else:
        DATA_DIR = P.LOCAL_LFW_ROOT
        FILE_LIST = P.LOCAL_LFW_LIST
    OUT_DIR = os.path.join(P.LOCAL_OUTDIR, OUT_DIR)
else:
    if DATASET_NAME == 'celeba':
        DATA_DIR = P.CELEBA_ROOT
        FILE_LIST = P.CELEBA_LIST
    else:
        DATA_DIR = P.LFW_ROOT
        FILE_LIST = P.LFW_LIST
    OUT_DIR = os.path.join(P.OUTDIR, OUT_DIR)

flags = tf.app.flags
flags.DEFINE_integer("epoch", EPOCHS, "Epoch to train [25]")
flags.DEFINE_float("learning_rate", LEARNING_RATE, "Learning rate of for adam [0.0002]")
flags.DEFINE_float("beta1", BETA1, "Momentum term of adam [0.5]")
flags.DEFINE_integer("train_size", np.inf, "The size of train images [np.inf]")
flags.DEFINE_integer("batch_size", BATCH_SIZE, "The size of batch images [64]")
flags.DEFINE_integer("image_size", IMAGE_SIZE, "The size of image to use")
flags.DEFINE_string("dataset", DATA_DIR, "Dataset directory.")
flags.DEFINE_string("filelist", FILE_LIST, "Pickle file containing list of dataset files")
flags.DEFINE_integer("val_size", VAL_SIZE, "Validation dataset size")
flags.DEFINE_integer("seed", SEED, "Random seed for dataset shuffle")
flags.DEFINE_string("checkpoint_dir", os.path.join(OUT_DIR, "checkpoint"), "Directory name to save the checkpoints [checkpoint]")
flags.DEFINE_string("sample_dir", os.path.join(OUT_DIR, "samples"), "Directory name to save the image samples [samples]")
flags.DEFINE_string("log_dir", os.path.join(OUT_DIR, "logs"), "Log directory")
FLAGS = flags.FLAGS

print('output directory:', OUT_DIR)
if not os.path.exists(FLAGS.checkpoint_dir):
    os.makedirs(FLAGS.checkpoint_dir)
    print('create new directory:', FLAGS.checkpoint_dir)
if not os.path.exists(FLAGS.sample_dir):
    os.makedirs(FLAGS.sample_dir)
    print('create new directory:', FLAGS.sample_dir)
if not os.path.exists(FLAGS.log_dir):
    os.makedirs(FLAGS.log_dir)
    print('create new directory:', FLAGS.log_dir)

# Copy source file to output directory
SOURCE_FILES = ['train-dcgan.py', 'complete.py', 'model.py', 'utils.py', 'ops.py', 'dataloader.py']
SRC_DIR = os.path.join(OUT_DIR, 'sourcecode')
if not os.path.exists(SRC_DIR) or not os.path.isdir(SRC_DIR):
    os.mkdir(SRC_DIR)
for f in SOURCE_FILES:
    copy2(os.path.join('./', f), os.path.join(SRC_DIR, f))
# Write description if any
if len(DESCRIPTION) > 0:
    with open(os.path.join(SRC_DIR, 'description.txt'), 'w') as f:
        f.write(DESCRIPTION)

start_time, start_time_ = time.time(), time.ctime()
with open(os.path.join(OUT_DIR, 'time.txt'), 'w') as f:
    f.write("start\t: " + start_time_ + "\n")

config = tf.ConfigProto()
config.gpu_options.allow_growth = True
with tf.Session(config=config) as sess:
    dcgan = DCGAN(sess, image_size=FLAGS.image_size, batch_size=FLAGS.batch_size,
                  is_crop=False, checkpoint_dir=FLAGS.checkpoint_dir)

    dcgan.train(FLAGS, paperspace_metrics=PAPERSPACE_METRICS, ps_metrics_interval=PS_METRICS_INTERVAL)

duration, ends_ = time.time() - start_time, time.ctime()
with open(os.path.join(OUT_DIR, 'time.txt'), 'a') as f:
    f.write("end\t: " + ends_ + "\n")
    f.write("Duration\t: {:2d}h {:2d}m {:7.4f}s\n".
            format(int(duration // 3600), int((duration % 3600) // 60), duration % 60))

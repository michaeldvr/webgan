import numpy as np
import os
import shutil


def create_model_config(model_dir, outdir):
    model_config = {
        "val_size": 2000,
        "epochs": 20,
        "learning_rate": 0.0002,
        "train_size": np.inf,
        "batch_size": 64,
        "image_size": 64,
        "checkpoint_dir": os.path.join(model_dir, "checkpoint"),
        "sample_dir": os.path.join(model_dir, "samples"),
        "log_dir": os.path.join(model_dir, "logs"),
        'approach': 'adam', 'lr': 0.01, 'beta1': 0.9, 'beta2': 0.999, 'eps': 1e-8,
        'hmcBeta': 0.2, 'hmcEps': 0.001, 'hmcL': 100, 'hmcAnneal': 1,
        'nIter': 350, 'imgSize': 64, 'lam': 0.1, 'outInterval': 50,
        'maskType': 'center', 'centerScale': 0.25, 'testSize': 5000, 'seed': 2014,
        'randomMask': 0.2,
        'imgs': [],
        'outDir': outdir
    }
    return model_config


def make_object(config_dict):
    class Config(object):
        def __init__(self, conf):
            for k in config_dict.keys():
                self.__setattr__(k, config_dict[k])
        def set_outDir(self, newval):
            self.outDir = newval
    return Config(config_dict)


def copytree(src, dst, symlinks=False, ignore=None):
    for item in os.listdir(src):
        s = os.path.join(src, item)
        d = os.path.join(dst, item)
        if os.path.isdir(s):
            shutil.copytree(s, d, symlinks, ignore)
        else:
            shutil.copy2(s, d)


def load_model_class(model_dir, outdir='./sourcecode'):
    copytree(os.path.join(model_dir, "sourcecode"), outdir)


if __name__ == '__main__':
    pass

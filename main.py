#!/usr/bin/env python
from flask import Flask, render_template, url_for, json, abort
import flask
from PIL import Image
import io
import base64
import helper
import numpy as np
import os
import uuid
import imageio
import model_loader as cc
import sys
import tensorflow as tf
from skimage.measure import compare_psnr, compare_ssim
sys.path.append(os.path.join('./', 'sourcecode'))
from sourcecode import model
import argparse

DEBUG = True
LOAD_MODEL = True  # utk debug
MODEL_DIR = './model'
COMPLETION_DIR = './completed'
SOURCECODE_DIR = './sourcecode'
ROUNDING = 3
ITERATION = 200
LEARNING_RATE = 0.01
HOST = "127.0.0.1"
PORT = "5000"

app = Flask(__name__)
app.config['MAX_CONTENT_LENGTH'] = 16 * 1024 * 1024
dcgan = None
sess = None
config = None
isLoaded = False

@app.route('/hello/<name>')
def hello(name=None):

    return render_template('main2.html', name=name)

@app.route('/')
def root():
    img = url_for('static', filename='placeholder.png')
    return render_template('main.html', placeholder=img)

@app.route('/preprocess', methods=['POST'])
def preprocess():
    # read body
    log(flask.request.files)
    image = flask.request.files['image'].read()
    image = Image.open(io.BytesIO(image))

    # img = to_base64(np.array(image))
    # return render_template('preprocess.html', img=img)

    image = prepare_img(image)

    rotation = [0, 3, 1, 2]
    rot = 0
    err = True
    result = None
    # log("saved:", save_np(image))
    while rot <= 3 and err:
        try:
            log("rotation", rotation[rot])
            args = helper.AlignConfig(np.rot90(image, rotation[rot]), size=64)
            result = np.array(helper.alignMain(args))
            log("check", result[0].shape)
            if result[0].shape[0] != 64: 
                raise Exception
            err = False
        except Exception:
            rot += 1
    if not err and result is not None:
        img = to_base64(result[0])
        # return render_template('preprocess.html', img=img)
        fn = save_np(result[0])
        data = {'img': img, 'np': fn}
        response = app.response_class(
            response=json.dumps(data),
            status=200,
            mimetype='application/json'
        )
        return response
    else:
        abort(500)


@app.route('/inpaint', methods=['POST'])
def inpaint():
    global dcgan
    global config
    log(flask.request.files)
    image = flask.request.form['image'] # filename
    mask_size = int(flask.request.form['mask_size'])
    mask = flask.request.form['mask']
    mask = [] if mask == '' else sorted([int(k) for k in mask.split(',')])
    log('received')
    log(mask_size)
    log(mask)
    log(load_np(image).shape)
    config.set_outDir(os.path.join('./completed', image))
    custom_mask = create_mask(mask_size, mask)
    dcgan.complete(config, [os.path.join('./preprocessed', image+'.png')], custom_mask=custom_mask)
    return 'oke'


@app.route('/collect', methods=['POST'])
def collect():
    fn = flask.request.form['image'] # name
    folder = os.path.join('./completed', fn)
    data = {'status': 'error', 'text': 'file not found'}
    if os.path.exists(folder) and os.path.isdir(folder):
        origin = imageio.imread(os.path.join('./preprocessed', fn + '.png'))
        hats_dir = os.path.join(folder, 'hats_imgs')
        inpt_dir = os.path.join(folder, 'completed')
        masked = os.path.join(folder, 'masked 1-1.png')
        hats = [os.path.join(hats_dir, f) for f in os.listdir(path=hats_dir)]
        hats = sorted(filter(lambda f: os.path.isfile(f), hats))[-1]
        inpt = [os.path.join(inpt_dir, f) for f in os.listdir(path=inpt_dir)]
        inpt = sorted(filter(lambda f: os.path.isfile(f), inpt))[-1]
        res_masked = imageio.imread(masked)
        res_inpt = imageio.imread(inpt)
        res_hats = imageio.imread(hats)
        psnr = compare_psnr(origin, res_inpt, data_range=255)
        ssim = compare_ssim(origin, res_inpt, data_range=255, multichannel=True)
        data = {
            'masked': to_base64(res_masked),
            'hats': to_base64(res_hats),
            'inpt': to_base64(res_inpt),
            'psnr': np.round(psnr, decimals=ROUNDING),
            'ssim': np.round(ssim, decimals=ROUNDING),
            'status': 'oke'
        }
    response = app.response_class(
        response=json.dumps(data),
        status=200,
        mimetype='application/json'
    )
    return response




@app.route('/tes_error')
def tes_error():
    abort(418)


def to_base64(data):
    img = Image.fromarray(data, 'RGB')
    buffer = io.BytesIO()
    img.save(buffer,format="JPEG")
    myimage = buffer.getvalue()
    jpeg = "data:image/jpeg;base64,"+base64.b64encode(myimage).decode('utf-8')
    log('shape', data.shape)
    return jpeg


def prepare_img(img):
    if img.mode != 'RGB':
        img.convert('RGB')
    return np.array(img)


def save_np(arr):
    files = os.listdir(path='./preprocessed')
    fn = uuid.uuid4().hex
    while fn in files:
        fn = uuid.uuid4().hex
    # np.save(os.path.join('./preprocessed', fn), arr)
    path = os.path.join('./preprocessed', fn)
    imageio.imwrite(path + '.png', arr)
    return fn


def load_np(fn):
    # return np.load(os.path.join('./preprocessed', fn + '.npy'))
    return imageio.imread(os.path.join('./preprocessed', fn + '.png'))


def create_mask(size, arr):
    tmp = np.ones((size, size))
    for idx in arr:
        i = (idx - 1) // size
        j = (idx - 1) % size
        tmp[i, j] = 0.0
    scale = 64 // size
    tmp_ones = np.ones((scale, scale))
    mask = np.kron(tmp, tmp_ones)
    return np.repeat(mask.reshape((64, 64, 1)), 3, axis=2)


def load_model():
    global dcgan
    global sess
    global config
    if sess is None:
        sess = tf.Session()
    conf = cc.create_model_config(MODEL_DIR, COMPLETION_DIR)
    conf["nIter"] = ITERATION
    # conf["imgs"].append(os.path.join('preprocessed', FILENAME_PP))
    conf["batch_size"] = 1
    conf["seed"] = None
    conf["beta1"] = 0.5
    conf["lr"] = LEARNING_RATE
    conf["maskType"] = 'center'  # center, grid, left, random
    conf['randomMask'] = 0.2
    config = cc.make_object(conf)
    dcgan = model.DCGAN(sess, image_size=config.image_size, batch_size=config.batch_size,
                        checkpoint_dir=config.checkpoint_dir, lam=config.lam)
    isLoaded = dcgan.load(config.checkpoint_dir)
    log(isLoaded)

def log(*args, **kwargs):
    if DEBUG:
        print(*args, **kwargs)

def run(args):
    global ITERATION
    global LEARNING_RATE
    global HOST
    global PORT
    global DEBUG
    global LOAD_MODEL
    ITERATION = args.iteration
    LEARNING_RATE = args.learning_rate
    HOST = args.host
    PORT = args.port
    DEBUG = args.debug
    LOAD_MODEL = not args.skip_model
    try:
        if LOAD_MODEL:
            log("loading model...")
            load_model()
        app.run(host=HOST, port=PORT)
    except KeyboardInterrupt:
        pass
    finally:
        log("close session")
        global sess
        if sess is not None:
            sess.close()

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Image Inpainting GAN Demo")
    parser.add_argument('--host', default="127.0.0.1", type=str, help="hostname")
    parser.add_argument('--port', default="5000", type=str, help="port number")
    parser.add_argument('--iteration', metavar="N", default=500, type=int, help="iterasi inpainting")
    parser.add_argument('--learning_rate', metavar="lr", default=0.01, type=float, help="adam learning rate")
    parser.add_argument('--debug', action='store_true', default=False, help='print debug log')
    parser.add_argument('--skip_model', action='store_true', default=False, help='skip load model (tdk bisa inpainting)')
    args = parser.parse_args()
    run(args)
